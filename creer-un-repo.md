# Le commencement

## Mise au point sur le vocabulaire
Pour commencer, il est impératif de faire la différence entre gitlab et git.

### Git
Git est un outil, à l'origine en ligne de commande, c'est ce que l'on va utiliser depuis chez nous.

### Dépôt (Repository)
Le dépôt c'est le projet hébergé sur un serveur (souvent le même que Gitlab).

### Gitlab
C'est une interface web permettant de présenter le projet git. C'est Gitlab qui contient le dépôt de votre projet. On peut y retrouver des concepts avancés que nous allons zapper pour l'instant.

## Comment créer un dépôt
Depuis Gitlab, vous pouvez vous rendre dans `Projects > New Project`. Vous donnez alors un nom à votre projet et choisissez un niveau de visibilité. Pensez à le mettre en privé pour les projets notés si vous ne voulez pas vous faire accuser de triche !

## Le principe de base
Un utilisateur peut "cloner" un dépôt, il dispose alors d'une version du projet sur son ordinateur. Il peut faire des modifications sans changer le code sur le dépôt, puis les envoyer une fois qu'il est content.
Pour commencer, vous allez donc cloner votre projet. Sur la page principale, vous pouvez trouver le bouton `Clone` qui vous propose deux methodes pour cloner votre dépôt. Je vous invite à vous renseigner sur la méthode SSH, mais la méthode HTTPS marchera tout aussi bien pour commencer. Vous copiez l'adresse, et l'intégrez à la commande :
```
git clone <adresse>
```
Vous avez alors un nouveau dossier qui se créé dans votre répertoire courant portant le nom de votre projet.

Bravo ! Vous avez créé et cloné votre premier dépôt !
