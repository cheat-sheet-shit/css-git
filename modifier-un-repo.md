# Modification d'un dépôt
Vous avez cloné votre premier dépôt et vous avez créé le fichier `main.cpp`.

## Ajouter un fichier à tracer
Git part du principe que tous les fichiers que vous avez créé ne font pas parti du projet à sauvegarder par défaut, il faut alors lui dire que ces fichiers doivent être tracés. Pour ce faire on utilise la commande :
```
git add <TARGET>
```
Ici <TARGET> peut être remplacé par un fichier ou un dossier.
Dans mon cas avec mon fichier `main.cpp`, je peux taper la commande :
```
git add main.cpp
```

### Attention
A chaques fois qu'un fichier a été modifié, si les modifications doivent être prises en compte il faut les ajouter. En réalité, il y a une zone de transite (staging area) qui signale à git ce qui doit être considéré au moment de créer la nouvelle version.

## Créer une nouvelle version
Pour déclarer une nouvelle version, nous allons faire un "commit" avec la commande :
```
git commit
```
Votre fenêtre devrait alors se changer en éditeur de texte, chaques version possède au minimum un titre et optionnellement une description.

Le titre se place sur la première ligne, et la description optionnelle à partir de la 3ème ligne.

### Problème avec l'éditeur
Peut-être que cette commande va vous ouvrir `vi`, c'est un éditeur de texte très utilisé sur Linux mais pas très intuitif, si vous êtes bloqués dans l'éditeur appuyez sur les touches `<escape>:q!<enter>` pour quitter sans sauvegarder. Si vous voulez changer ce comportement, vous pouvez définir la variable d'environment `EDITOR`.

### Fix
Pour le faire temporairement :
```
export EDITOR=nano
```
et pour le faire durablement :
```
echo "export EDITOR=nano" >> ~/.bashrc
```

## La commande status
Pour savoir où vous en êtes, il y a la commande:
```
git status
```
On y retrouve des informations très utiles, je vous laisse regarder, ça peut toujours vous être utile !

## Pour aller plus loin
La commande `git add` et `git commit` possèdent beaucoup de paramètres. Vous pouvez les voir tous à l'aide de la commande :
```
man git add
# et
man git commit
```

Mais voici les plus utiles

### Ajouter tous les fichiers
```
git add -A
```

### Voir les modifications avant
Git va vous demander si vous voulez ajouter à la zone de transite chaques modifications en vous montrant ce qui a été ajouté et supprimé.
```
git add -p
```

### Faire un commit sans description
```
git commit -m "Mon Super Titre"
```

### Ajouter tous les fichiers modifiés dans la zone de transite
```
git commit -a
```

## Envoyer les modifications
On peut finalement envoyer nos modifications sur le dépôt GitLab :
```
git push
```

Vous avez donc fait votre premier push, vous avez toutes les clés en main pour continuer votre projet en gardant une trace des évolutions!
